module lock_memory (
input l,
input a,
output t	
);
and (and1, l, a);
not (not1, l);
and(and2, and1, and1);
and(and3, not1, t);
or(t, and2, and3);

endmodule
